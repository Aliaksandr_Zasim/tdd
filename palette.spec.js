describe('Palette', function () {
	describe('#hexToRgb', function () {
		it('return "rgb(0,0,0)" with value "#000000"', function () {
			var palette = new Palette(),
				hex = '#000000',
				result = palette.hexToRgb(hex);
			result.should.equal('rgb(0,0,0)');
		});

		it('return "rgb(0,0,0)" with value "  #000000  "', function () {
			var palette = new Palette(),
				hex = '  #000000 ',
				result = palette.hexToRgb(hex);
			result.should.equal('rgb(0,0,0)');
		});

		it('return "rgb(0,0,0)" with value "000000"', function () {
			var palette = new Palette(),
				hex = '000000',
				result = palette.hexToRgb(hex);
			result.should.equal('rgb(0,0,0)');
		});

		it('return "rgb(0,0,0)" with value "  000000"  ', function () {
			var palette = new Palette(),
				hex = '  000000  ',
				result = palette.hexToRgb(hex);
			result.should.equal('rgb(0,0,0)');
		});

		it('return "rgb(0,0,0)" with value "  000"  ', function () {
			var palette = new Palette(),
				hex = '  000  ',
				result = palette.hexToRgb(hex);
			result.should.equal('rgb(0,0,0)');
		});

		it('return "rgb(0,0,0)" with value "000"  ', function () {
			var palette = new Palette(),
				hex = '  000  ',
				result = palette.hexToRgb(hex);
			result.should.equal('rgb(0,0,0)');
		});

		it('throw error "Incorrect value" with value "#tttttt"', function () {
			var palette = new Palette(),
				hex = '#tttttt';
			expect( function() {palette.hexToRgb(hex)} ).to.throw("Incorrect value");
		});

		it('throw error "Incorrect value" with value "#"', function () {
			var palette = new Palette(),
				hex = '#tttttt';
			expect( function() {palette.hexToRgb(hex)} ).to.throw("Incorrect value");
		});

		it('throw error "Incorrect value" with value "#1233"', function () {
			var palette = new Palette(),
				hex = '#tttttt';
			expect( function() {palette.hexToRgb(hex)} ).to.throw("Incorrect value");
		});

		it('throw error "Incorrect value" with value "1233"', function () {
			var palette = new Palette(),
				hex = '#tttttt';
			expect( function() {palette.hexToRgb(hex)} ).to.throw("Incorrect value");
		});
	});

	describe('#rgbToHex', function () {
		it('return #ffffff with value "rgb(255, 255, 255)"', function () {
			var palette = new Palette(),
				rgb = 'rgb(255, 255, 255)',
				result = palette.rgbToHex(rgb);
			result.should.equal('#ffffff');
		});

		it('return #ffffff with value "rgb(255,255,255)"', function () {
			var palette = new Palette(),
				rgb = 'rgb(255,255,255)',
				result = palette.rgbToHex(rgb);
			result.should.equal('#ffffff');
		});

		it('return #ffffff with value "  rgb(255,255,255)"', function () {
			var palette = new Palette(),
				rgb = '  rgb(255,255,255)',
				result = palette.rgbToHex(rgb);
			result.should.equal('#ffffff');
		});

		it('throw error "Incorrect value" with value "rgb(-1, 0, 1)"', function () {
			var palette = new Palette(),
				rgb = 'rgb(-1, 0, 1)';
			expect( function() {palette.rgbToHex(rgb)} ).to.throw("Incorrect value");
		});

		it('throw error "Incorrect value" with value "rgb(290, 0, 1)"', function () {
			var palette = new Palette(),
				rgb = 'rgb(-1, 0, 1)';
			expect( function() {palette.rgbToHex(rgb)} ).to.throw("Incorrect value");
		});
	});
});