var Palette = function () {
	this.hexToRgb = function (hex) {
		hex = hex.replace(/\s+/g, '');
		if(hex.charAt(0) === '#') {
			hex = hex.slice(1);
		}
		if((hex.length !== 3 && hex.length !== 6) || isNaN(parseInt(hex, 16))) {
			throw new Error('Incorrect value');
		} else {
			var result = 'rgb(';
			if (hex.length === 6) {
				result += parseInt(hex.slice(0, 2), 16) + ',';
				result += parseInt(hex.slice(2, 4), 16) + ',';
				result += parseInt(hex.slice(4), 16);
			} else {
				result += parseInt(hex.charAt(0) + hex.charAt(0)) + ',';
				result += parseInt(hex.charAt(1) + hex.charAt(1)) + ',';
				result += parseInt(hex.charAt(2) + hex.charAt(2));
			}
			return result + ')';
		}
	}

	this.rgbToHex = function (rgb) {
		rgb = rgb.replace(/\s+/g,'');
		if(!/^rgb\((0|[1-9]\d|1\d\d|2([0-4]\d|5[0-5])),(0|[1-9]\d|1\d\d|2([0-4]\d|5[0-5])),(0|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\)$/.test(rgb)) {
			throw new Error('Incorrect value');
		} else {
			rgb = rgb.replace(/rgb\(|\)/, '');
			rgb = rgb.match(/\d+/g);
			var result = "#";
			rgb.forEach(function (item) {
				result += parseInt(item).toString(16);
			});
			return result;
		}
	}
};